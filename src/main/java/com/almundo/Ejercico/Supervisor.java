package com.almundo.Ejercico;

public class Supervisor extends Employee {

    public Supervisor(String name) {
        super(name);
    }

    public void addEmployeeToCallCenter() {
        Dispatcher.getInstance().addAvaibleSupervisor(this);
    }
}
