package com.almundo.Ejercico;

public class Director extends Employee {

    public Director(String name) {
        super(name);
    }

    public void addEmployeeToCallCenter() {
        Dispatcher.getInstance().addAvaibleDirector(this);
    }
}
