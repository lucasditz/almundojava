package com.almundo.Ejercico;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PhoneCall implements Runnable {
    private final String id;
    private boolean taken;

    public PhoneCall(String id) {
        this.id = id;
        this.taken = false;
    }

    public String getId() {
        return id;
    }

    public synchronized void setTaken() {
        this.taken = true;
        notify();
    }

    /**
     * if the phone call cant be dispatched, wait until its attended by an employee.
     * Else, spleep until Dispatcher can manage it
     */
    public void run() {
        synchronized (this) {
            if (Dispatcher.getInstance().canTakePhoneCall(this)) {
                while (!this.taken) {
                    try {
                        wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(PhoneCall.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                try {
                    while (!Dispatcher.getInstance().canTakePhoneCall(this)) {
                        System.out.println("The call can't be processed. " + this.id + " will try in 5 seconds.");
                        Thread.sleep(5000);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(PhoneCall.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
