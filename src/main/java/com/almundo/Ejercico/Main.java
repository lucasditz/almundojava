package com.almundo.Ejercico;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class for test
 *
 */
public class Main 
{

    public static void main(String[] args) {
        // Init Dispatcher
        Dispatcher dispatcher = new Dispatcher();

        // Employees initialization
        int max_operators = 6;
        ArrayList<Operator> operators = new ArrayList<Operator>();
        int max_supervisors = 1;
        ArrayList<Supervisor> supervisors = new ArrayList<Supervisor>();
        int max_directors = 1;
        ArrayList<Director> directors = new ArrayList<Director>();

        // Call Center initialization
        CallCenter callCenter = new CallCenter();
        new Thread(callCenter).start();

        // Start employees threads
        for (int i = 0; i < max_operators; i++) {
            operators.add(new Operator("Operator" + i));
            new Thread(operators.get(i)).start();
        }
        for (int j = 0; j < max_supervisors; j++) {
            supervisors.add(new Supervisor("Supervisor" + j));
            new Thread(supervisors.get(j)).start();
        }
        for (int z = 0; z < max_directors; z++) {
            directors.add(new Director("Director" + z));
            new Thread(directors.get(z)).start();
        }

        // Generate phone calls
        int count = 1;
        for (;;) {
            for (int i = 1; i <= 10; i++) {
                PhoneCall phoneCall = new PhoneCall("PhoneCall_" + count);
                count++;
                new Thread(phoneCall).start();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
