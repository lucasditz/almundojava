package com.almundo.Ejercico;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Dispatcher {

    public final int MAX_CALLS = 10;
    private ArrayList<PhoneCall> phoneCalls = new ArrayList<PhoneCall>(MAX_CALLS);

    private LinkedList<Operator> operators;
    private LinkedList<Supervisor> supervisors;
    private LinkedList<Director> directors;

    private static Dispatcher instance = new Dispatcher();

    public Dispatcher() {
        operators = new LinkedList<Operator>();
        supervisors = new LinkedList<Supervisor>();
        directors = new LinkedList<Director>();
    }

    public static Dispatcher getInstance() {
        return instance;
    }

    /**
     * add operator to employees avaible
     * 
     * @param Employee e
     */
    public synchronized void addAvaibleOperator(Operator e) {
        // System.out.println("EMPLOYEE ADD: " + e.getName());
        operators.add(e);
        notify();
    }

    /**
     * add supervisor to employees avaible
     * 
     * @param Supervisor e
     */
    public synchronized void addAvaibleSupervisor(Supervisor s) {
        // System.out.println("Supervisor ADD: " + s.getName());
        supervisors.add(s);
        notify();
    }

    /**
     * add director to employees avaible
     * 
     * @param Employee e
     */
    public synchronized void addAvaibleDirector(Director d) {
        // System.out.println("Director ADD: " + d.getName());
        directors.add(d);
        notify();
    }

    /**
     * If reached dispatcher limit return false so the call sleep. Else add to incoming phone call
     * 
     * @param call
     * @return
     */
    public synchronized boolean canTakePhoneCall(PhoneCall call) {
        if (phoneCalls.size() == MAX_CALLS) {
            return false;
        }
        System.out.println("Income Phone call waiting for dispatch: " + call.getId());
        phoneCalls.add(call);
        notify();
        return true;
    }

    /**
     * if not phonecall wait. Else return a phone call.
     * 
     * @return PhoneCall
     */
    public synchronized PhoneCall getPhoneCall() {
        while (phoneCalls.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(CallCenter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // System.out.println("Phone call taken: " + phoneCalls.get(0).getId());
        return phoneCalls.remove(0);
    }

    /**
     * if not employee avaible wait. Else return the first employee in the priority queue
     * 
     * @return priority employee avaible
     */
    public synchronized Employee getAvailableEmployee() {
        while (operators.isEmpty() && supervisors.isEmpty() && directors.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(CallCenter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Employee employee = operators.poll();
        if (employee == null)
            employee = supervisors.poll();
        if (employee == null)
            employee = directors.poll();

        // System.out.println("Employee taken: " + employee.getName());
        return employee;
    }

    public void dispatchCall() {

        PhoneCall phoneCall = instance.getPhoneCall();
        Employee employee = instance.getAvailableEmployee();
        System.out.println("Phone call " + phoneCall.getId() + " has been dispatched to " + employee.getName());
        employee.takeCall(phoneCall);

    }

}
