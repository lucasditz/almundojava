package com.almundo.Ejercico;

import java.util.Timer;
import java.util.TimerTask;

//Abstract class because it can contains abstract methods that are different for each inherited classes
public abstract class Employee implements Runnable {
    protected String name;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void addEmployeeToCallCenter();

    public class SimulatedAttendedCall extends TimerTask {

        Timer timer;
        PhoneCall phoneCall;
        int randomCallDuration;

        public SimulatedAttendedCall(Timer timer, int randomCallDuration, PhoneCall phoneCall) {
            this.timer = timer;
            this.randomCallDuration = randomCallDuration;
            this.timer = timer;
            this.phoneCall = phoneCall;
        }

        @Override
        public void run() {
            phoneCall.setTaken();
            System.out.println("The call: " + phoneCall.getId() + " has been taken by " + name + " with a duration of "
                    + randomCallDuration + " seconds.");

            addEmployeeToCallCenter();
            timer.cancel();

        }

    }

    public void takeCall(PhoneCall call) {
        // Random call duration between 5 and 10 seconds
        int randomCallDuration = (int) Math.floor(Math.random() * (10 - 5 + 1) + 5);
        Timer timer = new Timer();
        // task will be scheduled after 5 sec delay
        timer.schedule(new SimulatedAttendedCall(timer, randomCallDuration, call), randomCallDuration * 1000);
    }

    public void run() {
        addEmployeeToCallCenter();
    }

}
