package com.almundo.Ejercico;

public class Operator extends Employee {

    public Operator(String name) {
        super(name);
    }

    public void addEmployeeToCallCenter() {
        Dispatcher.getInstance().addAvaibleOperator(this);
    }

}
